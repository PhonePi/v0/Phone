use std::process::Command;

fn main() {
let disable_output = Command::new("/usr/lib/ofono/test/disable-modem")
        .output()
        .expect("failed to execute process");

println!("{:?}", disable_output);

let poweroff_output = Command::new("systemctl")
        .arg("poweroff")
        .output()
        .expect("failed to execute process");

println!("{:?}", poweroff_output);
}
