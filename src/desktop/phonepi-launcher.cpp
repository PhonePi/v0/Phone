//
// Created by abdullin on 9/1/17.
//

#include <string>
#include <unistd.h>

const std::string weston = "dbus-launch --exit-with-session weston-launch";
const std::string desktop = "dbus-launch desktop-pi";

int main(int argc, char** argv) {
    if (fork() == 0) {
        sleep(3);
        system(desktop.c_str());
    }

    system(weston.c_str());
    return 0;
}
