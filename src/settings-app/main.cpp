#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QSlider>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QScreen>

#include <iostream>
#include <QtWidgets/QPushButton>
#include <thread>
#include <sstream>
#include <fstream>

#include "cerebro.h"
#include "Timer.h"

QLabel* temperatureValLabel;
QLabel* batteryValLabel;

#define SET_FONT(WIDGET, FONT) \
WIDGET->setStyleSheet("QLabel { color : black; }"); \
QFont FONT = WIDGET->font(); \
FONT.setPointSize(16); \
FONT.setBold(true); \
WIDGET->setFont(FONT); \

template <typename T>
QString toString(T val) {
    std::ostringstream ss;
    ss << val;
    return std::move(QString(ss.str().c_str()));
}

QString getOperator() {
    std::ifstream in("/usr/share/phonepi/info");
    std::string op;
    in >> op;
    if (not in) return "Error";
    auto column = op.find(':');
    return QString(op.substr(column + 1).c_str());
}

void temperatureUpdater(QString) {
    temperatureValLabel->setText(toString(getTemperature()) + "°C");
}

void batteryUpdater(QString) {
    batteryValLabel->setText(toString(getBattery()) + "%");
}

int main(int argc, char** argv) {
    QApplication a(argc, argv);
    QWidget widget;


    Timer secondTimer(1000);
    // start detached timer thread, that updates time label
    std::thread timerThread([&secondTimer] () -> void {secondTimer.start();});
    timerThread.detach();

    Timer minuteTimer(60 * 1000);
    // start detached timer thread, that updates time label
    std::thread minuteTimerThread([&minuteTimer] () -> void {minuteTimer.start();});
    minuteTimerThread.detach();
    // start detached brightness thread, that sets the brightness
    std::thread brightnessThread([]() -> void {brightness();});
    brightnessThread.detach();


    // main layout
    QVBoxLayout *layout = new QVBoxLayout();


    // time
    QLabel* timeLabel = new QLabel();
    timeLabel->setText(getCurrentTime().c_str());
    SET_FONT(timeLabel, timeFont);
    timeLabel->setAlignment(Qt::AlignCenter);
    QObject::connect(&secondTimer, &Timer::updateTime, timeLabel, &QLabel::setText);
    layout->addWidget(timeLabel, 0, Qt::AlignTop | Qt::AlignVCenter | Qt::AlignBaseline);

    // operator
    QWidget* operatorWidget = new QWidget;
    operatorWidget->setWindowTitle("operator");
    QHBoxLayout *operatorLayout = new QHBoxLayout();
    operatorWidget->setLayout(operatorLayout);

    QLabel* operatorLabel = new QLabel("Operator: ");
    SET_FONT(operatorLabel, operatorFont);

    QLabel* operatorValLabel = new QLabel(getOperator());
    SET_FONT(operatorValLabel, operatorValFont);

    operatorLayout->addWidget(operatorLabel);
    operatorLayout->addWidget(operatorValLabel);

    layout->addWidget(operatorWidget, 0, Qt::AlignTop | Qt::AlignVCenter | Qt::AlignBaseline);

    // brightness
    QWidget* brightnessWidget = new QWidget;
    brightnessWidget->setWindowTitle("brightness");
    QHBoxLayout *brightnessLayout = new QHBoxLayout();
    brightnessWidget->setLayout(brightnessLayout);

    QLabel* brightnessLabel = new QLabel("Brightness:");
    SET_FONT(brightnessLabel, brightnessFont);

    QSpinBox *brightnessSpinBox = new QSpinBox;
    brightnessSpinBox->setReadOnly(true);
    brightnessSpinBox->setValue(100);

    QSlider* brightnessSlider = new QSlider(Qt::Horizontal, 0);
    brightnessSlider->setRange(0, 100);
    brightnessSlider->setValue(100);
    QObject::connect(brightnessSlider, &QSlider::valueChanged, brightnessSpinBox, &QSpinBox::setValue);
    QObject::connect(brightnessSlider, &QSlider::valueChanged, readBrightness);

    brightnessLayout->addWidget(brightnessLabel);
    brightnessLayout->addWidget(brightnessSlider);
    brightnessLayout->addWidget(brightnessSpinBox);

    layout->addWidget(brightnessWidget, 0, Qt::AlignTop | Qt::AlignVCenter | Qt::AlignBaseline);


    // battery
    QWidget* batteryWidget = new QWidget;
    batteryWidget->setWindowTitle("battery");
    QHBoxLayout *batteryLayout = new QHBoxLayout();
    batteryWidget->setLayout(batteryLayout);

    QLabel* batteryLabel = new QLabel("Battery: ");
    SET_FONT(batteryLabel, batteryFont);

    batteryValLabel = new QLabel(toString(getBattery()) + "%");
    SET_FONT(batteryValLabel, batteryValFont);

    batteryLayout->addWidget(batteryLabel);
    batteryLayout->addWidget(batteryValLabel);
    QObject::connect(&minuteTimer, &Timer::updateTime, batteryUpdater);

    layout->addWidget(batteryWidget, 0, Qt::AlignTop | Qt::AlignVCenter | Qt::AlignBaseline);

    // temperature
    QWidget* temperatureWidget = new QWidget;
    temperatureWidget->setWindowTitle("temperature");
    QHBoxLayout *temperatureLayout = new QHBoxLayout();
    temperatureWidget->setLayout(temperatureLayout);

    QLabel* temperatureLabel = new QLabel("Temperature: ");
    SET_FONT(temperatureLabel, temperatureFont);

    temperatureValLabel = new QLabel(toString(getTemperature()) + "°C");
    SET_FONT(temperatureValLabel, temperatureValFont);

    temperatureLayout->addWidget(temperatureLabel);
    temperatureLayout->addWidget(temperatureValLabel);
    QObject::connect(&secondTimer, &Timer::updateTime, temperatureUpdater);

    layout->addWidget(temperatureWidget, 0, Qt::AlignTop | Qt::AlignVCenter | Qt::AlignBaseline);

    // back button
    QPushButton *backButton = new QPushButton("back");
    SET_FONT(backButton, btnFont);
    QObject::connect(backButton, &QPushButton::clicked, &a, &QApplication::quit);
    layout->addWidget(backButton, 0, Qt::AlignVertical_Mask);
    layout->addWidget(new QWidget, 0, Qt::AlignCenter);


    // start application
    widget.setLayout(layout);
    QRect geometry = QGuiApplication::primaryScreen()->geometry();
    widget.setFixedSize(geometry.width(), geometry.height());
    widget.activateWindow();
	widget.show();

    return a.exec();
}
