//
// Created by kivi on 31.05.17.
//

#include <sys/socket.h>
#include <sys/un.h>

#include <cstdio>
#include <pthread.h>
#include <unistd.h>

#include "cerebro.h"

#define I2C_CMD_BRIGTH  0xDD;   // "Set display brightness" (async.)
#define I2C_CMD_BATTERY 0xBB    // "Check battery status" (frequency == 60 sec.)
#define I2C_CMD_TEMP    0x36    // "Get Temperature" (async.)

const char *socket_path = "hidden";
char brightnessValue = 100;

void readBrightness(int newVal) {
    brightnessValue = (char) newVal;
}

void brightness() {
    char oldBrightness = 0;
    while (true) {
        if (brightnessValue != oldBrightness) {
            if (brightnessValue != 0) {
                oldBrightness = brightnessValue;
                setBrightness(brightnessValue);
            }
        }
    }
}

void setBrightness(char val) {
    struct sockaddr_un addr;
    char buf[100];
    int fd;

    if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket error");
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);

    if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        perror("connect error");
    }

    buf[0] = I2C_CMD_BRIGTH;
    buf[1] = val;
    if (write(fd, buf, 2) != 2) {
        fprintf(stderr,"write error\n");
    }
}

int getBattery() {
    struct sockaddr_un addr;
    char buf[100];
    int fd;

    if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket error");
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);

    if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        perror("connect error");
    }

    buf[0] = I2C_CMD_BATTERY;
    if (write(fd, buf, 1) != 1) {
        fprintf(stderr,"write error\n");
    }
}

double getTemperature() {
    struct sockaddr_un addr;
    char buf[100];
    int fd;

    if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket error");
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);

    if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        perror("connect error");
    }

    buf[0] = I2C_CMD_TEMP;
    if (write(fd, buf, 1) != 1) {
        fprintf(stderr,"write error");
    }

    if (read(fd, buf, 2) != 2) {
        fprintf(stderr,"read error");
    }
    double temperature = buf[0] + ((buf[1]>>6)*0.25);
    return temperature;
}
