//
// Created by kivi on 30.06.17.
//

#ifndef UNBLOCK_PI_TIMER_H
#define UNBLOCK_PI_TIMER_H

#include <QObject>

class Timer : public QObject {
Q_OBJECT

signals:
    void updateTime(QString);

public:

    Timer(size_t period_) : period(period_) {}

    void start();

private:

    size_t period;

};

std::string getCurrentTime();


#endif //UNBLOCK_PI_TIMER_H
